package com.example.cbd.kafka;

import com.example.cbd.service.FraudService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.json.JSONParser;
import org.bson.json.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Consumer {

    @Autowired
    private FraudService service;

    @KafkaListener(topics = KakfaTopics.PAYMENT, groupId = "${spring.kafka.consumer.group-id}")
    public void listener(String message) throws JsonProcessingException {
        log.info("Message received {} ", message);

        ObjectMapper mapper = new ObjectMapper();
        JsonNode actualObj = mapper.readTree(message);

        var cardNumber = actualObj.get("cardNumber").asLong();
        service.findByCardNumber(cardNumber);
    }
}
