console.log("Starting producer...!");
import Kafka from 'node-rdkafka';

const broker = process.env.KAFKA_HOST || 'localhost:9092'

export default class Publicador {

    constructor() {
        this.stream = Kafka.Producer.createWriteStream({
            'metadata.broker.list' : broker}, { }, {  topic: 'payment' });
    }

    /**
     * Colocar un mensaje en el stream configurado
     * @param {*} message Mensaje dejado en cola
     */
    queueMessage(message){
        const success = this.stream.write(Buffer.from(JSON.stringify(message) ));
        if(success){
            console.log("Sending message to Fraud microservice...");
        } else {
            console.log("Resultado", result);
        }
    }

}


