package com.example.cbd.kafka;

public final class KakfaTopics {

    public static final String PAYMENT = "payment";
    public static final String FRAUD = "fraud";
}
