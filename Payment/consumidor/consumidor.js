console.log("Starting consumer...!");

import Kafka from 'node-rdkafka';

const broker = process.env.KAFKA_HOST || 'localhost:9092'

export default class Consumidor {

    constructor() {
        this.consumer = Kafka.KafkaConsumer({
            'metadata.broker.list' : broker,
            'group.id' : 'paymentConsumer'
        }, {});
    }
    
    consumerConnection() {
        this.consumer.connect();
        return this.consumer;
       /* consumer.on('ready', () => {
            console.log('Consumer is ready!!');
            consumer.subscribe(['fraud']);
            consumer.consume();
        }).on('data',(data) => {
            console.log( `Mensaje recibido: ${data.value}`);
        });*/
    }
}







