package com.example.cbd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FraudMicroserviceApplication {

	public static void main(String[] args) {

		SpringApplication.run(FraudMicroserviceApplication.class, args);
		System.out.println("FRAUD IS UP AND RUNNING !!");
	}

}
