package com.example.cbd.mongo;

import com.example.cbd.entity.CreditCard;
import java.io.Serializable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

public interface CreditCardRepository extends MongoRepository<CreditCard, Serializable> {

    CreditCard findByCardNumber(@Param("cardNumber")long number);
}
