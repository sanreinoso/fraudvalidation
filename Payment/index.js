import express from 'express';
import Publicador from './publicador/publicador.js'
import Consumidor from './consumidor/consumidor.js'

const app = express();
const port = process.env.PORT || 3000;

let publicador = new Publicador();
let consumidor = new Consumidor();

app.use(express.json())
app.post('/payment', (req, resp) =>{

    publicador.queueMessage(req.body);
    consumidor.consumerConnection().on('ready', function() {
        console.log('Consumer is ready!!');
        consumidor.consumer.subscribe(['fraud']);  
        consumidor.consumer.consume(); 
    })
    .on('data', function (data) { 
        try {
            return resp.send({data: JSON.parse(data.value)})

        } catch (error) {
            console.log(error.message);
        }});
});


app.listen(port, () => console.log(`Server listening on port ${port}`));