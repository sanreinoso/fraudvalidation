package com.example.cbd.controller;

import com.example.cbd.entity.CreditCard;
import com.example.cbd.service.FraudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController()
public class FraudController {

    @Autowired
    public FraudService service;

    @PostMapping("/save")
    public ResponseEntity<CreditCard> cardsList(@RequestBody CreditCard card) {

        CreditCard response = service.addCard(card);
        return new ResponseEntity<CreditCard>(response, HttpStatus.CREATED);
    }

    @GetMapping("/card/{cardNumber}")
    public ResponseEntity<CreditCard> cardsList(@PathVariable long cardNumber) {

        CreditCard response = service.findByCardNumber(cardNumber);
        return new ResponseEntity<CreditCard>(response, HttpStatus.OK);
    }
}
