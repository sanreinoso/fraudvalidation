package com.example.cbd.service;

import com.example.cbd.entity.CreditCard;
import com.example.cbd.kafka.Producer;
import com.example.cbd.mongo.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FraudService {

    @Autowired
    private CreditCardRepository repository;

    @Autowired
    private Producer producer;

    public CreditCard addCard(CreditCard card){
        return repository.save(card);
    }

    public CreditCard findByCardNumber(long cardNumber){
        CreditCard card = repository.findByCardNumber(cardNumber);
        if(card != null) {
            producer.sendMessage("{status:Rejected,response:Invalid card}");
        } else {
            producer.sendMessage("{status:Accepted,response:Valid card}");
        }

        return repository.findByCardNumber(cardNumber);
    }
}
